package com.vdc.tele_commuting.common.ucapi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.vdc.tele_commuting.common.ucapi.HttpClientResultBean;
import com.vdc.tele_commuting.common.ucapi.StringUtil;


/**
 * 	 @date 	      : 2010.04.22
 *   @author      : kim dong hyuck
 *   @see 		  : none
 * 	 @description : Web 서버로 부터 GET, POST 요청을 보내고 받기 위한 메쏘드 유틸리티 클래스이다.
 * 					org.apache.commons.httpclient 패키지에 의존한다.
 */
public class HttpClientUtil {
	
	private final static String DEFAULTCHARSET = "UTF-8";
	private final static String DEFAULTCONTENTTYPE = "application/json";
	// private final static String DEFAULTCHARSET = "ISO-8859-1";
	private final static int DEFAULTTIMEOUT = 10000;
	protected static Log log = LogFactory.getLog(HttpClientUtil.class);
	protected static long startTime = 0;	
	
	public static void setRequestHeaders(HttpRequestBase hmb, String rawHeaderString) {
		if(rawHeaderString == null || rawHeaderString.trim().equals("")) {
			return;
		}

		// rawHeaderString 을 파싱해서 org.apache.commons.httpclient.methods.GetMethod 에 Request Header로 넣는다.
		java.util.StringTokenizer st = new java.util.StringTokenizer(rawHeaderString, "\n");

		while(st.hasMoreTokens()) {
			String line = st.nextToken();
			if(line == null || line.trim().equals("")) {
				line = "";
			}

			String headerName = "";
			String headerValue = "";

			if(line.equals("") == false && line.indexOf(":") != -1) {
				headerName = line.substring(0, line.indexOf(":"));
				headerValue = line.substring(line.indexOf(":") + 1);

				headerName = headerName.trim();
				headerValue = headerValue.trim();
				
				hmb.addHeader(headerName, headerValue);
			}
		}
	}
	
	private static HttpEntity getBodyEntity(String strBody) {
		HttpEntity entity = null;
		try {
			final String body = strBody;
			ContentProducer cp = new ContentProducer() {
				public void writeTo(OutputStream outstream) throws IOException {
					//Writer writer = new OutputStreamWriter(outstream, "UTF-8");
					Writer writer = new OutputStreamWriter(outstream, DEFAULTCHARSET);
					writer.write(body);
					writer.flush();
				}
			};
			entity = new EntityTemplate(cp);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return entity;
	}
	
	protected static HttpRequestBase getMethod(String strMethod, String strUrl, String requestBody) {
		return getMethod(strMethod, strUrl, requestBody, false, null, 0);
	}
	
	@SuppressWarnings("deprecation")
	protected static HttpRequestBase getMethod(String strMethod, String strUrl, String requestBody, boolean chunked, InputStream stream, long contentLength) {
		String method = strMethod.toLowerCase();
		if("post".equals(method)){
			HttpPost httpPost = new HttpPost(strUrl);
			if(requestBody != null && !"".equals(requestBody )){
				HttpEntity entity = getBodyEntity(requestBody);
				httpPost.setEntity(entity);
			}
			return httpPost;
		} else if("put".equals(method)) {
			HttpPut httpPut = new HttpPut(strUrl);
			if(requestBody != null && !"".equals(requestBody )){
				HttpEntity entity = getBodyEntity(requestBody);
				httpPut.setEntity(entity);
			}
			if(stream != null && !"".equals(stream)){
				InputStreamEntity entity = new InputStreamEntity(stream, contentLength);
				entity.setChunked(chunked);
				httpPut.setEntity(entity);
			}
			
			return httpPut;
		} else if("delete".equals(method)) {
			return new HttpDelete(strUrl);
		} else {
			return new HttpGet(strUrl);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean download(
								String strMethod,
								String strUrl,
								Hashtable htRequestHeaders,
								String requestBody,
								String contentType){
		
		return download(strMethod, strUrl, htRequestHeaders, null, requestBody, DEFAULTTIMEOUT, false, DEFAULTCHARSET, DEFAULTCONTENTTYPE);
	}
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean download(
									String strMethod,
									String strUrl,
									Hashtable htRequestHeaders,
									Hashtable htRequestParameters,
									String requestBody,
									int nTimeout,
									boolean fFollowRedirect,
									String charSet,
									String contentType) {
		
		HttpClient httpclient = null;
		HttpRequestBase method = null;
		
		HttpClientResultBean resultBean = new HttpClientResultBean();

		try {
			// set timeout
			HttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, DEFAULTTIMEOUT);
			
			httpclient = new DefaultHttpClient(httpParams);

			// method를 가져오면서, requestBody도 세팅해준다.
			method = getMethod(strMethod, strUrl, requestBody);
			
			// post 인 경우는 follow redirect 불가
			if(strMethod.equalsIgnoreCase("post"))
				fFollowRedirect = false;

			setRequestHeaders(method, htRequestHeaders, charSet, contentType);

			// post 인 경우에만 parameter add가 가능
			if(strMethod.equalsIgnoreCase("post") && htRequestParameters != null) {
				Iterator<String> iter = htRequestParameters.keySet().iterator();
				StringBuffer sb = new StringBuffer();
				while(iter.hasNext()) {
					String key = iter.next();
					String value = (String)htRequestParameters.get(key);
					sb.append(key).append(": ").append(value).append("\n");
				}
				HttpEntity entity = new StringEntity(sb.toString());
				((HttpPost)method).setEntity(entity);
			}
			HttpResponse res = httpclient.execute(method);
			StatusLine sline = res.getStatusLine();
			int statusCode = sline.getStatusCode();
			HttpEntity entity = res.getEntity();
			if(entity != null) {
				resultBean.setResultInputStream(new BufferedInputStream(entity.getContent()));
			}
			resultBean.setResultCode(statusCode);
			resultBean.setStatusCode(statusCode);
		} catch(Exception e) {
			resultBean.setResultCode(-1);
            resultBean.setStatusCode(-1);
            resultBean.setResultString("ERR: " + StringUtil.getExceptionTrace(e));
		} finally {
			if(method != null) {
				httpclient.getConnectionManager().shutdown();
			}
		}
		
		return resultBean;
	}
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean getConnResult(
									String strMethod,
									String strUrl,
									Hashtable htRequestHeaders,
									Hashtable htRequestParameters,
									String requestBody,
									int nTimeout,
									boolean fFollowRedirect,
									String charSet,
									String contentType) {

setStartTime("start biz connection..................");
		
		HttpClient httpclient = null;
		HttpRequestBase method = null;
		
		HttpClientResultBean resultBean = new HttpClientResultBean();

		try {
			// set timeout
			HttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, DEFAULTTIMEOUT);
			
			httpclient = new DefaultHttpClient(httpParams);

			// method를 가져오면서, requestBody도 세팅해준다.
			method = getMethod(strMethod, strUrl, requestBody);
			
			// post 인 경우는 follow redirect 불가
			if(strMethod.equalsIgnoreCase("post"))
				fFollowRedirect = false;

			setRequestHeaders(method, htRequestHeaders, charSet, contentType);

			// post 인 경우에만 parameter add가 가능
			if(strMethod.equalsIgnoreCase("post") && htRequestParameters != null) {
				/*Iterator<String> iter = htRequestParameters.keySet().iterator();
				StringBuffer sb = new StringBuffer();
				while(iter.hasNext()) {
					String key = iter.next();
					String value = (String)htRequestParameters.get(key);
					//sb.append(key).append(": ").append(value).append("\n");
					sb.append(key).append("=").append(value).append("&");
				}
				HttpEntity entity = new StringEntity(sb.toString());
				((HttpPost)method).setEntity(entity);
				*/
				setRequestParameters((HttpPost)method, htRequestParameters, true);
			}
setEndTime("/connection", "end biz end ...........");
setStartTime("start execute method..................");
			HttpResponse res = httpclient.execute(method);
			StatusLine sline = res.getStatusLine();
			int statusCode = sline.getStatusCode();
			HttpEntity entity = res.getEntity();
			if(entity != null) {
				String body = EntityUtils.toString(entity);
				resultBean.setResultString(body);
			}
			resultBean.setResultCode(statusCode);
			resultBean.setStatusCode(statusCode);
		} catch(Exception e) {
			resultBean.setResultCode(-1);
            resultBean.setStatusCode(-1);
            resultBean.setResultString("ERR: " + StringUtil.getExceptionTrace(e));
		} finally {
			if(method != null) {
				httpclient.getConnectionManager().shutdown();
			}
		}		
setEndTime("get connection result", "end execute method..................");
		return resultBean;
	}
	
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean getResponseHeader(
			String strMethod,
			String strUrl,
			Hashtable htRequestHeaders,
			String requestBody,
			String contentType){
		
		return getResponseHeader(strMethod, strUrl, htRequestHeaders, requestBody, false, null, 0L, contentType);
	}
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean connUpload(
			String strMethod,
			String strUrl,
			Hashtable htRequestHeaders,
			InputStream stream, long contentLength, String contentType){
		
		return getResponseHeader(strMethod, strUrl, htRequestHeaders, null, true, stream, contentLength, contentType);
	}
	
	// upload 일때 chunked 명시
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean getResponseHeader(
								String strMethod,
								String strUrl,
								Hashtable htRequestHeaders,
								String requestBody, boolean chunked,
								InputStream stream, long contentLength, String contentType){
		
		HttpClient httpclient = null;
		HttpRequestBase method = null;
		
		HttpClientResultBean resultBean = new HttpClientResultBean();

		try {
			// set timeout
			HttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, DEFAULTTIMEOUT);
			
			httpclient = new DefaultHttpClient(httpParams);

			// method를 가져오면서, requestBody도 세팅해준다.
			method = getMethod(strMethod, strUrl, requestBody, chunked, stream, contentLength);
			
			setRequestHeaders(method, htRequestHeaders, DEFAULTCHARSET, contentType);
			
			HttpResponse res = httpclient.execute(method);
			int statusCode = res.getStatusLine().getStatusCode();
			
			resultBean.setResultCode(statusCode);
			resultBean.setStatusCode(statusCode);
			resultBean.setResponseHeaders(res.getAllHeaders());
		} catch(Exception e) {
			resultBean.setResultCode(-1);
            resultBean.setStatusCode(-1);
            resultBean.setResultString("ERR: " + StringUtil.getExceptionTrace(e));
		} finally {
			if(method != null) {
				httpclient.getConnectionManager().shutdown();
			}
		}
		
		return resultBean;
	}
	
	
	@SuppressWarnings("unchecked")
	public static HttpClientResultBean getConnResult(
								String strMethod,
								String strUrl,
								Hashtable htRequestHeaders,
								String requestBody){
		
		return getConnResult(strMethod, strUrl, htRequestHeaders, null, requestBody, DEFAULTTIMEOUT, false, DEFAULTCHARSET, DEFAULTCONTENTTYPE);
	}

	@SuppressWarnings("unchecked")
	public static void setRequestHeaders(HttpRequestBase hmb, Hashtable htRequestHeaders, String requestCharset, String contentType) {
		/* default header */
		if(requestCharset == null || "".equals(requestCharset)){
			requestCharset = DEFAULTCHARSET;
		}
		if(requestCharset == null || "".equals(requestCharset)){
			contentType = DEFAULTCONTENTTYPE;
		}
		
		if(htRequestHeaders != null) {
			for(Enumeration e = htRequestHeaders.keys(); e.hasMoreElements(); ) {
				String headerName = (String) e.nextElement();
				String headerValue = (String) htRequestHeaders.get(headerName);

				hmb.setHeader(headerName, headerValue);
			}
		}
		
		// 기본헤더
		hmb.setHeader("Content-Type", contentType + "; charset=" + DEFAULTCHARSET);	// post
		hmb.setHeader("Accept","*/*");
		hmb.setHeader("Accept-Charset", DEFAULTCHARSET);
		hmb.setHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; i-NavFourF; .NET CLR 1.1.4322)");		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public static void setRequestParameters(HttpPost postMethod, Hashtable htRequestParameters, boolean fKorCharEncoding) {
		if(htRequestParameters != null) {
			for(Enumeration e = htRequestParameters.keys(); e.hasMoreElements(); ) {
				String paramName = (String) e.nextElement();
				String paramValue = (String) htRequestParameters.get(paramName);

				if(fKorCharEncoding) {
					try {
						postMethod.getParams().setParameter(paramName, new String(paramValue.getBytes("euc-kr"), "iso-8859-1"));
					} catch(UnsupportedEncodingException uee) {
						postMethod.getParams().setParameter(paramName, paramValue);
					}
				} else {
					postMethod.getParams().setParameter(paramName, paramValue);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void setRequestParameters(HttpPost postMethod, Hashtable htRequestParameters) {
		setRequestParameters(postMethod, htRequestParameters, true);
	}

	/**
	 * HttpServletRequest 헤더로 부터 필요한 헤더만 추출해서 Hashtable 로 리턴해 준다.
	 * 필요한 헤더란, accept, accept-language, accept-encoding, user-agent, cookie 이다.
	 * @param request HttpServletRequest
	 * @return Hashtable
	 */
	@SuppressWarnings("unchecked")
	public static Hashtable retrieveMinimumRequestHeaders(HttpServletRequest request) {
		Hashtable ht = new Hashtable();

		String[] requiredHeaderNames = {"accept",
										"accept-language",
										"accept-encoding",
										"user-agent",
										"cache-control",
										"tomcatquery",
										"cookie"};

		String[] requiredHeaderDefaultValues = {"*/*",
												"ko",
												"gzip, deflate",
												"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)",
												"no-cache",
												"",
												""};

		for(int i = 0; i < requiredHeaderNames.length; i++) {
			String requiredHeaderName = requiredHeaderNames[i];
			String requiredHeaderDefaultValue = requiredHeaderDefaultValues[i];

			// HttpServletRequest 안에 있는 실제 헤더 값을 찾는다.
			String realHeaderValue = (request != null) ? request.getHeader(requiredHeaderName) : "";

			// 실제 헤더 값이 존재하지 않으면, 디폴트로 세팅한다.
			if(StringUtil.isEmptyStr(realHeaderValue)) {
				realHeaderValue = requiredHeaderDefaultValue;
			}

			// 헤더 정보를 Hashtable 안으로 세팅
			if(StringUtil.isEmptyStr(realHeaderValue) == false)
				ht.put(requiredHeaderName, realHeaderValue);
		}

		return ht;
	}

	/**
	 * HttpServletRequest 객체안에 있는 모든 파라메터를  param1=value1&param2=value2.. 형태의 문자열로 만들어 리턴
	 * @param request HttpServletRequest
	 * @param fURLEncodeForValue boolean
	 * @return String
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static String convertRequestParametersToQueryString(HttpServletRequest request, boolean fURLEncodeForValue) {
		StringBuilder sb = new StringBuilder();
		if(request != null) {
			for(Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
				String pn = (String) e.nextElement();
				String pv = request.getParameter(pn);

				if(fURLEncodeForValue)
					pv = java.net.URLEncoder.encode(pv);

				sb.append("&").append(pn).append("=").append(pv);
			}

			if(sb.toString().startsWith("&"))
				sb.substring(1);
		}

		return sb.toString();
	}

	public static String convertRequestParametersToQueryString(HttpServletRequest request) {
		return convertRequestParametersToQueryString(request, true);
	}
	
	private static void setStartTime(String msg) {
		if(log.isDebugEnabled()) {
			startTime = System.currentTimeMillis();			
			log.debug(msg);
		}
	}
	
	private static void setEndTime(String uri, String msg) {
		long endTime = System.currentTimeMillis() - startTime;
		int msce = (int) endTime % 1000;
		int second = (int) endTime / 1000;
		second = second % 60;
		
		if(log.isDebugEnabled()) {
			log.debug(msg + " [RunTime : " + second + "ss " + msce + "ms] - URI : " + uri);
		}
	}
	
}
