package com.vdc.tele_commuting.common.ucapi;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

  public static String getParam(String p, String defaultValue) {
    return (p == null || p.trim().equals("")) ? defaultValue : p.trim();
  }

  public static boolean isEmptyStr(String p) {
    return (p == null || p.trim().equals(""));
  }

  public static String getSelectedOption(String pordField, String matchkey,
                                         String dispName) {
    StringBuilder sb = new StringBuilder();

    sb.append("<option value=\"");
    sb.append(matchkey);
    sb.append("\"");

    if (pordField.equalsIgnoreCase(matchkey))
      sb.append(" selected>");
    else
      sb.append(">");

    sb.append(dispName);

    sb.append("</option>");

    String rtn = sb.toString();
    sb.setLength(0);
    sb = null;

    return rtn;
  }

  public static String getExceptionTrace(Exception ee) {
    String rtn = "";

    try {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);

      ee.printStackTrace(pw);

      rtn = sw.toString();

      pw.close();
      sw.close();

    }
    catch (IOException ioe) {
      rtn = "getExceptionTrace(Exception ee) : " + ioe.toString();
    }

    return rtn;
  }

  public static String getErrorTrace(Error err) {
    String rtn = "";

    try {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);

      err.printStackTrace(pw);

      rtn = sw.toString();

      pw.close();
      sw.close();

    }
    catch (IOException ioe) {
      rtn = "getExceptionTrace(Exception ee) : " + ioe.toString();
    }

    return rtn;
  }

  public static String toKor(String p) {
    return toKor(p, "euc-kr");
  }

  public static String toKor(String p, String encoding) {
    try {
      return new String(p.getBytes("8859_1"), encoding);
    }
    catch (UnsupportedEncodingException uee) {
      return uee.toString();
    }
  }

  /**
   * & < > " 를 인코딩한다. well-formed xml 이기 위해서
   */
  public static String xmlEncode(String str) {
	/*
     * 원문에 & < > " 를 포함하고 있으면, 각각 &amp; &lt; &gt; &quot; 로 인코딩 한다
     */

    if (str == null || str.trim().equals(""))
      str = "";

    // 변환 결과물을 담을 변수
    String cvt = str.trim();

    cvt = cvt.replaceAll("&", "&amp;");
    cvt = cvt.replaceAll("<", "&lt;");
    cvt = cvt.replaceAll(">", "&gt;");
    cvt = cvt.replaceAll("\"", "&quot;");

    return cvt;
  }

  /** 공통 페이지 xml 을 위한 변환 - HDML 용
  *
  * @param	str		xml-encoded Text
  *
  *  &amp;  --> &amp;amp;
  *  &lt;   --> &amp;lt;
  *  &gt;   --> &amp;gt;
  *  &quot; --> &amp;quot;
  *   $     --> &amp;dol;
  */
  public static String hdmlEncodeForCommonXml(String xmlEncodedStr) {
    if (xmlEncodedStr == null || xmlEncodedStr.trim().equals(""))
      xmlEncodedStr = "";

    String cvt = xmlEncodedStr.trim();

    cvt = replacement(cvt, "&amp;", "&amp;amp;");
    cvt = replacement(cvt, "&lt;", "&amp;lt;");
    cvt = replacement(cvt, "&gt;", "&amp;gt;");
    cvt = replacement(cvt, "&quot;", "&amp;quot;");
    cvt = replacement(cvt, "$", "&amp;dol;");

    return cvt;
  }

  public static String getLoginFailRedirectUrl(HttpServletRequest request, String errorUrl) {
    StringBuilder sb = new StringBuilder();

    sb.append("http://");
    sb.append(request.getServerName());

    if (request.getServerPort() != 80) {
      sb.append(":");
      sb.append(request.getServerPort());
    }

    sb.append(request.getContextPath());
    sb.append(errorUrl);

    return sb.toString();
  }

  public static String getAdultAuthFailRedirectUrl(HttpServletRequest request, String errorUrl) {
    StringBuilder sb = new StringBuilder();

	sb.append("http://");
    sb.append(request.getServerName());
	
    if (request.getServerPort() != 80) {
	    sb.append(":");
	    sb.append(request.getServerPort());
	}
	
	sb.append(request.getContextPath());
	sb.append(errorUrl);
	
	return sb.toString();
  }

  @SuppressWarnings("unchecked")
public static String getPublicServerIP() {
    String rtn = "";
    try {
      java.util.Enumeration e = java.net.NetworkInterface.getNetworkInterfaces();

      while (e.hasMoreElements()) {
        java.net.NetworkInterface netface = (java.net.NetworkInterface) e.
            nextElement();

        java.util.Enumeration e2 = netface.getInetAddresses();

        while (e2.hasMoreElements()) {
          java.net.InetAddress ip = (java.net.InetAddress) e2.nextElement();

          String tempIP = getParam(ip.toString(), "").trim();
          while (tempIP.startsWith("/"))
            tempIP = tempIP.substring(1);

          if (tempIP.toString().startsWith("211."))
            rtn = tempIP;
        }
      }
    }
    catch (Exception e) {
      rtn = e.toString();
    }

    return rtn;
  }

  public static String convertToJavascriptStr(String s) {
    String rtn = s;

    rtn = replacement(rtn, "\\", "\\\\");
    rtn = replacement(rtn, "\"", "\\\"");
    rtn = replacement(rtn, "\t", "\\\t");
    rtn = replacement(rtn, "\n", "\\\n");
    rtn = replacement(rtn, "\r", "\\\r");

    return rtn;
  }

  public static String getHtmlizedContent(String strCONTENT) {
    String s = strCONTENT;
    s = replacement(s, "\n", "<br/>");
    s = replacement(s, "\t", "&nbsp;&nbsp;");
    s = replacement(s, "\r", "");
    s = replacement(s, "\\", "￦");
    s = replacement(s, "\"", "&quot;");

    s = substring2(s, 25);
    return s;
  }
  
  public static String getHtmlizedContentNoCut(String strCONTENT) {
    String s = strCONTENT;
    s = replacement(s, "\n", "<br/>");
    s = replacement(s, "\t", "&nbsp;&nbsp;");
    s = replacement(s, "\r", "");
    s = replacement(s, "\\", "￦");
    s = replacement(s, "\"", "&quot;");

    return s;
  } 

  public static String getPageURL(HttpServletRequest request) {
    return getPageURL(request, false);
  }

  public static String getPageFullURL(HttpServletRequest
                                      request) {
    return getPageURL(request, true);
  }

  @SuppressWarnings("unchecked")
  public static String getPageURL(HttpServletRequest request,
                                  boolean fFull) {
    StringBuffer buff = new StringBuffer();

    if (fFull) {
      buff.append(getFullRootURLPath(request));
    }
    else {
      buff.append(getRootURLPath(request));
    }

    buff.append(request.getServletPath());

    if (request.getMethod().equalsIgnoreCase("GET")) {
      if (StringUtil.isEmptyStr(request.getQueryString()) == false) {
        buff.append("?");
        buff.append(request.getQueryString());
      }
    }
    else if (request.getMethod().equalsIgnoreCase("POST")) {

      int i = 0;
      for (java.util.Enumeration e = request.getParameterNames();
           e.hasMoreElements(); ) {
        String paramName = (String) e.nextElement();
        String paramValue = StringUtil.getParam(request.
            getParameter(paramName), "");

        paramValue = StringUtil.toKor(paramValue);

        buff.append( (i == 0) ? "?" : "&");
        buff.append(paramName);
        buff.append("=");

        try {
          buff.append(java.net.URLEncoder.encode(paramValue, "euc-kr"));
        }
        catch (UnsupportedEncodingException ex) {
        }

        i++;
      }

    }
    else {
    }

    return buff.toString();
  }

  /**
  *	특정 문자열을 검색하여 대치시킨다.
  *	대소문자를 구별하여 변경한다.
  *	@param	source		문자열의 원본
  *	@param	find			찾을 문자열
  *	@param	dest			변경할 문자열
  *	@return	변경후의 문자열
  */
  public static String
      replacement(
          String source,
          String find,
          String dest
      ) {
    return replacement(source, find, dest, 0);
  }

  /**
   *	특정 문자열을 검색하여 대치시킨다.
   *	대소문자를 구별하여 변경한다.
   *	@param	source			문자열의 원본
   *	@param	find				찾을 문자열
   *	@param	dest				변경할 문자열
   *	@param	startPosition		변경적용를 시작할 위치
   *	@return	변경후의 문자열
   */
  public static String
      replacement(
          String source,
          String find,
          String dest,
          int startPosition
      ) {
    if (source == null || source.length() <= 0) {
      return "";
    }

    if (find == null || find.length() <= 0) {
      return source;
    }

    if (dest == null) {
      dest = "";
    }

    String result = "";

    int first = 0;
    int next = 0;

    next = source.indexOf(find, startPosition);
    if (next < 0) {
      return source;
    }

    while (next >= startPosition) {
      result += source.substring(first, next);
      result += dest;

      first = next + find.length();
      next = source.indexOf(find, first);
    }

    if (first < source.length()) {
      result += source.substring(first);
    }

    return result;
  }

  /**
   *  문자열을 지정한 바이트 길이 만큼으로 잘라내는 함수
   *
   * @param		str			원본 문자열
   * @param		maxbytelen	자를 바이트 수
   * @return	지정된 바이트 길이 만큼 자른 문자열. 만약 원본 문자열이 지정한 바이트 길이 보다 작으면 원본 문자열을 그대로 리턴.
   */
  public static String getCutString(String str, int maxbytelen) { // str; maxbytelen����Ʈ ��ŭ8�� �߶��ش�.
    if (getByteLength(str) <= maxbytelen)
      return str;

    StringBuffer sb = new StringBuffer();
    int nCurrBytes = 0; // 현재까지의 바이트수
    for (int i = 0; i < str.length(); i++) {
      String ch = str.substring(i, i + 1); // get a char
   // 현재까지의 바이트수에 현 글자바이트수를 더해봐서 maxbytelen보다 작은지 검사
      if ( (nCurrBytes + ch.getBytes().length) <= maxbytelen) {
        sb.append(ch);
        nCurrBytes += ch.getBytes().length;
      }
      else
        break;
    }
    String rtn = sb.toString();
    //clear buffer
    sb.setLength(0);
    sb = null;
    return (rtn.substring(0, rtn.length() - 2) + "..");
  }

  /**
   * 문자열의 바이트 길이를 리턴
   *
   * @param		str		바이트 길이를 세고자 하는 문자열
   * @return	String객체가 null 이거나 "" 이면 0을, 그 외에는 문자열의 바이트 길이를 리턴
   */
  public static int getByteLength(String str) {
    return (str == null || str.equals("")) ? 0 : str.getBytes().length;
  }

  public static int getByteSize(String str){
	  int len = 0;	  
	  try{		  
		  int en = 0;
		  int ko = 0;
		  int etc = 0;
		 
		  char[] string = str.toCharArray();
		 
		  for (int j=0; j<string.length; j++) {
		      if (string[j]>='A' && string[j]<='z') {
		      	en++;
		      }else if (string[j]>='\uAC00' && string[j]<='\uD7A3') {
		      	ko++;
			   	ko++;
		      }else {
		       	etc++;
		      }
		  }
		  return (en + ko + etc);
		    
	  }catch(Exception e){		  
		 len = (str == null || str.equals("")) ? 0 : str.getBytes().length;
	  }
	  
	  return len;
  }
  
  public static String trimChar(String str, String ch) {
    if (str == null || str.equals(""))
      return "";

    String strChar = ch;
    while (str.startsWith(strChar))
      str = str.substring(1);

    while (str.endsWith(strChar))
      str = str.substring(0, str.length() - 1);

    return str;
  }

  public static String trimChar(String str, char ch) {
    return trimChar(str, "" + ch);
  }

  public static String trimQuote(String str) {
    return trimChar(str, "\"");
  }

  /**
   *  문자열 중 특수 문자를 지정한 Markup Language에 맞게 변환하여 리턴
   *  예) HTML 의 경우: < → &lt;  > → &gt; 등
   *  예) WML의 경우: $ → $$  HDML의 경우: $ → &dol;
   *
   * @param	str		원본 문자열
   * @param	ml		마크업랭귀지 (HTML, WML, HDML 세 값 중 하나)
   * @return	해당 마크업 랭귀지로 인코딩된 문자열
   */
  public static String mlEncode(String str, String ml) {
    if (str == null || str.trim().equals(""))
      return "";

    if (ml.toUpperCase().equals("WML")) {
      // WML
      str = replacement(str, "&amp;", "&");
      str = replacement(str, "&", "&amp;");

      str = replacement(str, "<", "&lt;");
      str = replacement(str, ">", "&gt;");
      str = replacement(str, "'", "&apos;");
      str = replacement(str, "\"", "&quot;");

      str = replacement(str, "$", "$$");

    }
    else if (ml.toUpperCase().equals("HDML")) {
      // HDML
      str = replacement(str, "&amp;", "&");
      str = replacement(str, "&", "&amp;");

      str = replacement(str, "<", "&lt;");
      str = replacement(str, ">", "&gt;");
      str = replacement(str, "\"", "&quot;");
      str = replacement(str, "$", "&dol;");
      str = replacement(str, "'", "&#39;");
    }
    else {
      // HTML
      str = replacement(str, "&amp;", "&");
      str = replacement(str, "&", "&amp;");

      str = replacement(str, "<", "&lt;");
      str = replacement(str, ">", "&gt;");
      str = replacement(str, "\"", "&quot;");
      //str = replacement(str, "'", "&#39;");


      str = replacement(str, "&amp;lt;", "&lt;");
      str = replacement(str, "&amp;gt;", "&gt;");
      str = replacement(str, "&amp;quot;", "&quot;");
      str = replacement(str, "&amp;amp;", "&amp;");
      str = replacement(str, "&amp;#39;", "&#39;");
    }

    return str;
  }

  public static String htmlEncode(String str) {
    return mlEncode(str, "HTML");
  }

  public static String wmlEncode(String str) {
    return mlEncode(str, "WML");
  }

  public static String hdmlEncode(String str) {
    return mlEncode(str, "HDML");
  }

  /**
   * 데이터베이스에 INSERT 할때 교체되어야 할 문자를 교체하는 함수
   * 현재는 ' 문자만 '' 로 변환되어 리턴
   *
   * @param	str		DB에 넣기전 원본 문자열
   * @return  DB 쿼리에 사용되어도 괜찮은 변환된 문자열
   */
  public static String dbEncode(String str) {
    return replacement(str, "'", "''");
  }

  /**
   * 문자열 라인으로 부터 Attribute의 값을 얻는다.
   *
   * @param	strLine		정보를 담고 있는 문자열 라인<br>
   * 문자열 라인은 다음과 같은 형태로 되어 있다고 가정한다.<br>
   * 예) attribute1=value1&attribute2=value2&...&attributeN=valueN...
   * @param	strAttributeName	라인으로 부터 찾고자 하는 attribute의 이름
   * @return	해당 attribute에 대응하는 값(value) 문자열.<br>
   * strLine 및 strAttributeName이 null 또는 "" 인 경우 "" 리턴.<br>
   * attribute를 strLine으로 못찾는 경우에도 "" 를 리턴
   */
  public static String getAttributeFromUrl(String url, String attr) {
    String strAttrKey = attr.toUpperCase() + "=";

    int nStartIndex = url.toUpperCase().indexOf(strAttrKey);
    if (nStartIndex == -1) // can't find attribute
      return "";

    nStartIndex += strAttrKey.length();

    /* url이 ~~~~~~&attr=  로 그냥 값없이 끝나는 경우
	    ^
	nStartIndex
	*/
    if (nStartIndex == url.length())
      return "";

    // 다음 attribute 시작 문자 & 를 찾는다.
    int nEndIndex = url.indexOf("&", nStartIndex);

    // 다음 attribute가 없다면
    // 예) ~~~~~~~&attr=value
    if (nEndIndex == -1)
      nEndIndex = url.length(); // to the end of the url line

    if (nStartIndex <= nEndIndex) {
      String rtn = "";

      try {
        rtn = java.net.URLDecoder.decode(url.substring(nStartIndex, nEndIndex),
                                         "euc-kr");
      }
      catch (UnsupportedEncodingException ex) {
      }

      return rtn;

    }
    else {
      return "";
    }
  }

  /**
   * int 타입의 숫자를 지정된 자리수의 문자로 만든다.<br>
   * 예) makeFixedSizeInt(100, 5) 는 "00100"를 리턴한다.<br>
   *
   * @param		intValue		변환 대상이 되는 숫자
   * @param		nSize			변환후의 길이, x 자리
   * @return		즉, 이 함수는 앞에 0 을 맞추어 넣어 준다. 예) makeFixedSizeInt(100, 5) 는 "00100"를 리턴
   * 만약 변환 대상 숫자의 길이가 만들기를 원하는 지정 길이보다 이미 클 경우에는 변환 대상 숫자를 문자열로만 바꾸어 리턴한다.
   */
  public static String makeFixedSizeInt(int intValue, int nSize) {
    return makeFixedSizeInt("" + intValue, nSize);
  }

  /**
   * int 타입의 숫자를 지정된 자리수의 문자로 만든다.<br>
   * 예) makeFixedSizeInt(100, 5) 는 "00100"를 리턴한다.<br>
   *
   * @param		strValue		변환 대상이 되는 숫자 (String type)
   * @param		nSize			변환후의 길이, x 자리
   * @return		즉, 이 함수는 앞에 0 을 맞추어 넣어 준다. 예) makeFixedSizeInt(100, 5) 는 "00100"를 리턴
   * 만약 변환 대상 숫자의 길이가 만들기를 원하는 지정 길이보다 이미 클 경우에는 변환 대상 숫자를 문자열로만 바꾸어 리턴한다.
   */
  public static String makeFixedSizeInt(String sValue, int nSize) {
    String strValue = sValue;

    int nCurSize = strValue.length();
    if (nCurSize >= nSize)
      return sValue;

    int diff = nSize - nCurSize;
    StringBuffer sb = new StringBuffer(nSize);

    for (int i = 0; i < diff; i++)
      sb.append("0");

    sb.append(strValue);

    String rtn = sb.toString().trim();
    sb.setLength(0);
    sb = null;

    return rtn;
  }

  /**
   * 코드로 이루어진 문자를 제거한다. 예: &#숫자; --> ""
   *
   */
  public static String removeCodeChar(String str) {
    return (str == null) ? "" : str.replaceAll("[&][#][0-9]+?[;]", "");
  }

  /**
   * 한중일 공통 한자 영역에 있는 한자(漢字)를 대체 문자로 교체한다.
   * 유니코드: 한중일 공통 한자 영역 ( 4E00~9FA5 )
   * 참고 사이트: http://www.javadom.com/java/messages/295.html
   *
   * @param		strText		문장
   * @param		strReplacement	 한자를 대체할 문자. 예: "" 로 지정하면 한자는 공백문자로 바뀐다.
   * @return		한자 대치된 문자열. strText가 null or "" 이면 "" 리턴
   */
  public static String replaceHanja(String strText, String strReplacement) {
    if (strText == null || strText.equals(""))
      return "";

    StringBuffer buff = new StringBuffer(strText.length());

    for (int i = 0; i < strText.length(); i++) {
      char ch = strText.charAt(i);

      int n = ch;
      if (n >= 0x4E00 && n <= 0x9FA5)
        buff.append(strReplacement);
      else
        buff.append(ch);

      //buff.append(Integer.toHexString(n) + "|");	// 문자의 유니코드를 찍어 보기 위함
    }

    String rtn = buff.toString();
    buff.setLength(0);
    buff = null;

    return rtn;
  }

  /**
   * 한자를 제거한다.
   * 내부적으로 return replaceHanja(strText, ""); 을 호출한다.
   */
  public static String removeHanja(String strText) {
    return replaceHanja(strText, "");
  }

  /** xmlEncode와 같다 */
  public static String htmlEncodeForCommonXml(String str) {
    return xmlEncode(str);
  }

  /** 공통 페이지 xml 을 위한 변환 - WML 용
  *
  * @param	xmlEncodedStr		xml-encoded Text
  *
  *  &amp;  --> &amp; (변함 없음)
  *  &lt;   --> &lt;  (변함 없음)
  *  &gt;   --> &gt;  (변함 없음)
  *  &quot; --> &quot; (변함 없음)
  *   '     --> &apos;
  *   $     --> $$
  */
  public String wmlEncodeForCommonXml(String xmlEncodedStr) {
    if (xmlEncodedStr == null || xmlEncodedStr.trim().equals(""))
      xmlEncodedStr = "";

    String cvt = xmlEncodedStr.trim();

    cvt = replacement(cvt, "'", "&apos;");
    cvt = replacement(cvt, "$", "$$");

    return cvt;
  }

  /** 공통 페이지 xml 을 위한 변환 - html, hdml, wml 을 지정해서 변환
  *
  * @param	xmlEncodedStr		xml-encoded text
  * @param	sMarkup				어떤 markup? html | hdml | wml 류 중에 하나
  */
  public String encodeTextForCommonXml(String xmlEncodedStr, String sMarkup) {
    if (xmlEncodedStr == null || xmlEncodedStr.trim().equals(""))
      xmlEncodedStr = "";

    String cvt = xmlEncodedStr.trim();

    if (sMarkup.toLowerCase().indexOf("hdml") != -1) {
      // hdml
      return hdmlEncodeForCommonXml(xmlEncodedStr);
    }
    else if (sMarkup.toLowerCase().indexOf("wml") != -1) {
      // wml
      return wmlEncodeForCommonXml(xmlEncodedStr);
    }
    else {
      // html
      return cvt;
    }
  }

  public String getDisplayPhoneKey(String pstrPhoneKey) {
    if (pstrPhoneKey == null || pstrPhoneKey.trim().equals(""))
      return "N/A";

    if (pstrPhoneKey.startsWith("82"))
      pstrPhoneKey = pstrPhoneKey.substring(2);

    if (pstrPhoneKey.length() == 11) {
      pstrPhoneKey = pstrPhoneKey.substring(0, 3) + "-" +
          pstrPhoneKey.substring(3, 7) + "-" + pstrPhoneKey.substring(7);
    }
    else if (pstrPhoneKey.length() == 10) {
      pstrPhoneKey = pstrPhoneKey.substring(0, 3) + "-" +
          pstrPhoneKey.substring(3, 6) + "-" + pstrPhoneKey.substring(6);
    }
    else {

      if (pstrPhoneKey.indexOf("_airnet019") != -1) {
        pstrPhoneKey = "LGT: " +
            pstrPhoneKey.substring(0, pstrPhoneKey.indexOf("_airnet019"));
      }
    }

    return pstrPhoneKey;
  }

  public static String substring2(String str, int max) {
    if (str == null)
      return "";
    return ( (str.length() > max) ? substring(str, max - 1) : str);
  }

  public static String getFullURLPath(HttpServletRequest request) {
    return getRootURLPath(request, true);
  }

  public static String getFullRootURLPath(HttpServletRequest request) {
    return getRootURLPath(request, true);
  }

  public static String getRootURLPath(HttpServletRequest request) {
    return getRootURLPath(request, false);
  }

  public static String getRootURLPath(HttpServletRequest request,
                                      boolean fIncludeHost) {
    StringBuffer sb = new StringBuffer();

    sb.append("http://");
    sb.append(request.getServerName());

    if (request.getServerPort() != 80) {
      sb.append(":" + request.getServerPort());
    }

    sb.append(request.getContextPath());

    return sb.toString();
  }

  /**
   * replace some html codes.
   *
   * @param contents String original contents.
   * @return String revised contents.
   */
  public static String replaceHtmlCodes(String contents) {
    // html code to be replaced.
    String htmlCode = "&";
    String htmlCode2 = "<";
    String htmlCode3 = ">";
    String htmlCode4 = "\"";

    // replacement.
    String codeReplacement = "&amp;";
    String codeReplacement2 = "&lt;";
    String codeReplacement3 = "&gt;";
    String codeReplacement4 = "&quot;";

    int indexReplacement = contents.indexOf(htmlCode);
    if (indexReplacement != -1) {
      contents = contents.replaceAll(htmlCode, codeReplacement);
    }

    int indexReplacement2 = contents.indexOf(htmlCode2);
    if (indexReplacement2 != -1) {
      contents = contents.replaceAll(htmlCode2, codeReplacement2);
    }

    int indexReplacement3 = contents.indexOf(htmlCode3);
    if (indexReplacement3 != -1) {
      contents = contents.replaceAll(htmlCode3, codeReplacement3);
    }

    int indexReplacement4 = contents.indexOf(htmlCode4);
    if (indexReplacement4 != -1) {
      contents = contents.replaceAll(htmlCode4, codeReplacement4);
    }

    // carrage return value.
    char[] newLineChar = new char[1];
    newLineChar[0] = (char) 0x000A;
    String newLine = new String(newLineChar);

    int index = contents.indexOf(newLine);
    if (index != -1) {
      contents = contents.replaceAll(newLine, "<BR>");
    }

    return contents;
  }

  /**
   * get the substring with the proper length.
   *
   * @param str String input string.
   * @param max int max. length of the substring.
   * @return String
   */
  public static String substring(String str, int max) {
    if (str == null) {
      return "";
    }

    int length = str.length();

    String SUFFIX = "...";

    int limit = length;
    if (length > max) {
      limit = max;
    }
    else {
      SUFFIX = "";
    }

    str = str.substring(0, limit);

    str += SUFFIX;

    return str;
  }

  /**
   * 문자열 라인으로 부터 Attribute의 값을 얻는다.
   *
   * @param	strLine		정보를 담고 있는 문자열 라인<br>
   * 문자열 라인은 다음과 같은 형태로 되어 있다고 가정한다.<br>
   * 예) attribute1=value1&attribute2=value2&...&attributeN=valueN...
   * @param	strAttributeName	라인으로 부터 찾고자 하는 attribute의 이름
   * @return	해당 attribute에 대응하는 값(value) 문자열.<br>
   * strLine 및 strAttributeName이 null 또는 "" 인 경우 "" 리턴.<br>
   * attribute를 strLine으로 못찾는 경우에도 "" 를 리턴
   */
  public static String getAttributeFromLine(String strLine,
                                            String strAttributeName) {
    if (strLine == null || strLine.trim().equals("")) {
      return "";
    }
    if (strAttributeName == null || strAttributeName.trim().equals("")) {
      return "";
    }

    int nStartIndex = -1;
    int nEndIndex = -1;

    String strAttrKey = strAttributeName + "=";
    strAttrKey = strAttrKey.toUpperCase();

    nStartIndex = strLine.toUpperCase().indexOf(strAttrKey);
    if (nStartIndex == -1) { // can't find Attribute
      return "";
    }

    nEndIndex = strLine.toUpperCase().indexOf("&", nStartIndex + 1);
    if (nEndIndex == -1) {
      nEndIndex = strLine.length();
    }

    if (nStartIndex < nEndIndex) {
      return strLine.substring(nStartIndex + strAttrKey.length(), nEndIndex);
    }

    return "";
  }

  /**
   * request 객체를 활용해서 지정된 쿠키 값을 얻어온다.
   *
   * @param	request		HttpServletRequest 객체
   * @param	name		얻고자 하는 쿠키 Key
   * @return	해당 쿠키 Key에 대응하는 쿠키 값. 만약 해당 쿠키 Key 가 없으면 "" 을 리턴한다.
   */
  public static String getCookie(HttpServletRequest request,
                                 String name) {
    javax.servlet.http.Cookie[] cookies = request.getCookies();
    String returnCookie = "";

    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        if (cookies[i].getName().equals(name)) {
          returnCookie = cookies[i].getValue();
          break;
        }
      }
    }
    return returnCookie;
  }

  public static String getResultSetDumpHtmlString(java.sql.ResultSet resultSet) {
    if (resultSet == null)
      return "Your ResultSet is null!";

    try {
      java.sql.ResultSetMetaData rsmd = resultSet.getMetaData();

      StringBuffer buff = new StringBuffer();
      int cols = rsmd.getColumnCount();

      buff.append("<table border=1>").append("\n");

      buff.append("<tr>").append("\n");
      for (int i = 0; i < cols; i++) {
        buff.append("<td>" + rsmd.getColumnName( (i + 1)) +
            "</td>").append("\n");
      }
      buff.append("</tr>").append("\n");

      while (resultSet.next()) {
        buff.append("<tr>").append("\n");
        for (int i = 0; i < cols; i++) {
          buff.append("<td>" + resultSet.getString( (i + 1)) +
              "</td>").append("\n");
        }
        buff.append("</tr>").append("\n");
      }
      buff.append("</table>").append("\n");

      return buff.toString();
    }
    catch (java.sql.SQLException sqle) {
      return getExceptionTrace(sqle);
    }
  }

  public static String getWebOutput( String str, String defaultValue, int trimLen, boolean fRemoveHtml )
  {
    String rtn = getParam( str, defaultValue );
    rtn = substring2( rtn, trimLen );

    return ( fRemoveHtml ) ? replaceHtmlCodes( rtn ) : rtn;
  }

  public static String getWebOutput( String str, String defaultValue, int trimLen )
  {
    return getWebOutput(  str,  defaultValue,  trimLen, true);
  }

  public static int getParamInt( HttpServletRequest req, String paramName, int defaultValue )
  {
    int nValue = defaultValue;
    String sValue = getParam( req.getParameter(paramName), "" + defaultValue);

    try {
      nValue = Integer.valueOf(sValue).intValue();
    }
    catch (Exception e) {
      nValue = defaultValue;
    }

    return nValue;
  }

  public static long getParamLong( HttpServletRequest req, String paramName, long defaultValue )
  {
    long nValue = defaultValue;
    String sValue = getParam( req.getParameter(paramName), "" + defaultValue);

    try {
      nValue = Long.valueOf(sValue).longValue();
    }
    catch (Exception e) {
      nValue = defaultValue;
    }

    return nValue;
  }

  public static String getParamStr( HttpServletRequest req, String paramName, String defaultValue )
  {
    String nValue = defaultValue;
    String sValue = getParam( req.getParameter(paramName), defaultValue);

    if( sValue == null ) {
      nValue = defaultValue;
    } else {
    	nValue = sValue;
    }

    return nValue;
  }
  
  public static String stackTraceToString(Throwable ex) {
      ByteArrayOutputStream b = new ByteArrayOutputStream();
      PrintStream p = new PrintStream(b);
      ex.printStackTrace(p);
      p.close();
      String stackTrace = b.toString();
      try {
          b.close();
      } catch (IOException e) {
          e.printStackTrace();
      }

      return convertHtmlBr(stackTrace);
  }
  
  public static String convertHtmlBr(String comment) {
      if (comment == null)
          return "";
      int length = comment.length();
      StringBuffer buffer = new StringBuffer();
      for (int i = 0; i < length; i++) {
          String tmp = comment.substring(i, i + 1);
          if ("\r".compareTo(tmp) == 0) {
              tmp = comment.substring(++i, i + 1);
              if ("\n".compareTo(tmp) == 0)
                  buffer.append("<br>\r");
              else
                  buffer.append("\r");
          }
          buffer.append(tmp);
      }
      return buffer.toString();
  }
  
  public static Map<String, String> convertQueryStrToMap(String str) {
	  Map<String, String> map = new java.util.HashMap<String, String>();
	  String[] txt_list = str.split("&");
	  int txt_len = txt_list.length;
	  for(int i=0; i<txt_len; i++) {
		  String txt = txt_list[i];
		  if("".equals(txt)) {
			  continue;
		  }
		  String[] tmp_list = txt.split("=");
		  int tmp_len = tmp_list.length;
		  if(tmp_len > 1) {
			  map.put(tmp_list[0], tmp_list[1]);
		  }
	  }
	  return map;
  }
  
  public static String convertStrListToDBParamList(String strList) {
	  String retStr = "";
	  String[] str_list = strList.split(",");
	  int len = str_list.length;
	  for(int i=0; i<len; i++) {
		  String str = str_list[i].trim();
		  if("".equals(str)) {
			  continue;
		  }
		  if("".equals(retStr)) {
			  retStr = "'" + str + "'";
		  } else {
			  retStr = retStr + "," + "'" + str + "'";
		  }
	  }
	  
	  return retStr;
  }
    
  public static boolean validateDomain(String domain) {
	  boolean isValidate = false;
	  
	  /* email 이 32bytes 넘어가면 응답 없음
	  String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";

	  isValidate = Pattern.matches(regex, email);
	  */
	  
	  try {
		  String domainPattern = "(?:\\w+\\.)+\\w+$";
		  isValidate = Pattern.matches(domainPattern, domain);

	  } catch (Exception e) {
		  isValidate = false;
	  }

	  return isValidate;
  }
  
  
  public static String sortStr(String txt, String sep) {
	// convert string to array
	String[] str_list = txt.split(sep);
	
	// sort data
	Arrays.sort(str_list);
	
	// convert array to string
	int len = str_list.length;
	StringBuffer sb = new StringBuffer();
	for(int i=0; i<len; i++) {
		String tmp = str_list[i];
		
		if(sb.length() > 0) sb.append(sep);
		sb.append(tmp);
	}
	
	return sb.toString();
  }
  
  public static String removeSpecialStr(String str) {
	  String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
	  str = str.replaceAll(match, "");
	  return str;
  }
  
  public static String convertStrListToStr(List<String> list, boolean isSkip) {
	  if(list == null || list.isEmpty()) return "";
	  
	  StringBuffer sb = new StringBuffer();
	  
	  Iterator<String> iter = list.iterator();
	  while(iter.hasNext()) {
		  String str = iter.next().trim();
		  if(isSkip && "".equals(str)) continue;
		  
		  if(sb.length() > 0) sb.append(",");
		  sb.append(str);
	  }
	  
	  return sb.toString();
  }
  /*
  public static String convertListToStr(List<Map<String, Object>> list, String key, boolean isSkip) {
	  return convertListToStr(list, key, null, null, isSkip);
  }

  public static String convertListToStr(List<Map<String, Object>> list, String key1, String key2, String sep, boolean isSkip) {
	  if(list == null || list.isEmpty()) return "";
	  
	  boolean isGetSub = (key2 == null || "".equals(key2)) ? false : true;
	  if(sep == null || "".equals(sep)) sep = "|";
	  
	  StringBuffer sb =new StringBuffer();
	  
	  Iterator<Map<String, Object>> iter = list.iterator();
	  while(iter.hasNext()) {
		  Map<String, Object> map = iter.next();
		  String str1 = CommonUtils.nullToSpace(String.valueOf(map.get(key1)));
		  String str2 = (isGetSub) ? CommonUtils.nullToSpace(String.valueOf(map.get(key2))) : null;
		  
		  if(isSkip && "".equals(str1)) continue;
		  
		  if(sb.length() > 0) sb.append(",");
		  sb.append(str1);
		  if(isGetSub) sb.append(sep).append(str2);
	  }
	  
	  return sb.toString();
  }*/
  
}
