package com.vdc.tele_commuting.common.ucapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@Service("baseService")
public class BaseService {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	protected long startTime = 0;

/*	@Autowired
	private MessageSourceAccessor messageSourceAccessor = null;
	
    protected final MessageSourceAccessor getMessageSourceAccessor() {
        return messageSourceAccessor;
    }
    */
	protected void setStartTime(String msg) {
		if(log.isDebugEnabled()) {
			startTime = System.currentTimeMillis();			
			log.debug(msg);
		}
	}
	
	protected void setEndTime(String uri, String msg) {
		long endTime = System.currentTimeMillis() - startTime;
		int msce = (int) endTime % 1000;
		int second = (int) endTime / 1000;
		second = second % 60;
		
		if(log.isDebugEnabled()) {
			log.debug(msg + " [RunTime : " + second + "ss " + msce + "ms] - URI : " + uri);
		}
	}
	

}
