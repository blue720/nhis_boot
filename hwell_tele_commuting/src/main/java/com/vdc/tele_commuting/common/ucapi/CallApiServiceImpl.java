package com.vdc.tele_commuting.common.ucapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



@Service("callApiService")
public class CallApiServiceImpl extends BaseService implements CallApiService {

	@Value("${presence.callforward}")
	private String sendUrl;

	public int sendPresence_TBCTAB03(List<String> conn_list) {

		int r = 0;
		try {
			Map<String, Object> ucMap = new HashMap<>();
			ucMap.put("list", conn_list);

			log.info("###conn_list:" + conn_list);

			ObjectMapper om = new ObjectMapper();
			String jsonStr = om.writeValueAsString(ucMap);

			log.info("###jsonStr:" + jsonStr);

			String strUrl = sendUrl;//"http://173.1.20.181:8080/uc.server.presence/callforward/time.json";

			log.info("sendURL: "+ sendUrl);

			HttpClientResultBean hb = HttpClientUtil.getConnResult("POST", strUrl, null, jsonStr);
			Map<String, Object> m = null;


			if(hb != null){
				m = om.readValue(hb.getResultString(), new TypeReference<Map<String, Object>>(){});
			}

			log.info("######## sendPresence_TBCTAB03 json to object : " +  m != null ? m.get("result") : "");
			if(m.containsKey("result") || (Boolean) m.get("result")) {
				r = 1;
			}
		}catch(Exception e) {
			log.info(e.getMessage() );
		}
		return r;
	}

}
