package com.vdc.tele_commuting.common.ucapi;

import java.io.BufferedInputStream;

import org.apache.http.Header;


/**
 * 	 @date 	      : 2010.04.22
 *   @author      : park han   
 *   @see 		  : BaseBean
 * 	 @description : 서버간 HTTP통신후 결과값에 대한 데이터를 정의한 Bean Class 
 */
@SuppressWarnings("serial")
public class HttpClientResultBean extends BaseBean{
	
	private int resultCode;
	private int statusCode;
	private String resultString;	
	private BufferedInputStream resultInputStream;
	
	public BufferedInputStream getResultInputStream() {
		return resultInputStream;
	}

	public void setResultInputStream(BufferedInputStream resultInputStream) {
		this.resultInputStream = resultInputStream;
	}

	private Header[] responseHeaders;
	
	public Header[] getResponseHeaders() {
		return responseHeaders;
	}

	public void setResponseHeaders(Header[] responseHeaders) {
		this.responseHeaders = responseHeaders;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public int getResultCode() {
		return resultCode;
	}

	public String getResultString() {
		return resultString;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

}
