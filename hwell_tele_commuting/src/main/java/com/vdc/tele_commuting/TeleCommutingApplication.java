package com.vdc.tele_commuting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeleCommutingApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeleCommutingApplication.class, args);
    }

}
