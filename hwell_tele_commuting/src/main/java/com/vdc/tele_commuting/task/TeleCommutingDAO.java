package com.vdc.tele_commuting.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import com.vdc.tele_commuting.common.dao.AbstractDAO;

import javax.annotation.Resource;


@Repository("teleCommutingDAO")
public class TeleCommutingDAO extends AbstractDAO{

    @Resource(name="sqlSessionMain")
    private SqlSessionTemplate Session;



    @SuppressWarnings("unchecked")
    public List<Map<String,Object>> getZ06Data() throws Exception{
        Map<String,Object> map = new HashMap<String,Object>();
        return selectList(Session,"jaeteak.TBCTAZ06_get", map);
    }

    public void setTBCTAB03(Map<String,Object> map) throws Exception{
        insert(Session,"jaeteak.TBCTAB03_CallType0_Insert", map);
        insert(Session,"jaeteak.TBCTAB03_CallType1_Insert", map);
    }

    public void setProcY(Map<String,Object> map) throws Exception{
        insert(Session,"jaeteak.TBCTAZ06_Proc_Update", map);
    }

}
