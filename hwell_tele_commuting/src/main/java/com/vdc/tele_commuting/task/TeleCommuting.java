package com.vdc.tele_commuting.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.vdc.tele_commuting.common.ucapi.CallApiService;

@Component
public class TeleCommuting {


    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name="teleCommutingService")
    private TeleCommutingService teleCommutingService;

    @Resource(name="callApiService")
    private CallApiService callApiService;

    //fixedDelay
    @Scheduled(fixedDelayString = "${Scheduled.fixedDelay}") //300000 1000 * 60 * 5
    public void Go() throws Exception {

        try {

            List<Map<String,Object>> Z06Data = teleCommutingService.getZ06Data();
            List<String> telno_list = new ArrayList<String>();

            //1. TBCTAB03 외부 내부 insert
            //2. TBCTAZ06.PROC_YN = 'Y' 처리
            //3. tel_no_list 작성
            for(int i = 0; i < Z06Data.size(); i++){

                teleCommutingService.setTBCTAB03(Z06Data.get(i));
                telno_list.add(Z06Data.get(i).get("TEL_NO").toString());
                teleCommutingService.setProcY(Z06Data.get(i));

            }

            logger.info("Z06Data.size: "+Z06Data.size());

            // uc presence 후출
            if (Z06Data.size() > 0) {

                logger.info("###telno_list :" + telno_list);
                int r = callApiService.sendPresence_TBCTAB03(telno_list);
                logger.info("###sendPresence_TBCTAB03 return :" + r);
            }


        }catch(Exception e) {
            e.printStackTrace();
        }


    }

}
