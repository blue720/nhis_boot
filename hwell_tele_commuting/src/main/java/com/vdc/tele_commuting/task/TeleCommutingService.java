package com.vdc.tele_commuting.task;

import java.util.List;
import java.util.Map;

public interface TeleCommutingService {
	
	List<Map<String,Object>> getZ06Data()throws Exception;
	void setTBCTAB03(Map<String,Object> map) throws Exception;
	void setProcY(Map<String,Object> map) throws Exception;
	
	
	
}
