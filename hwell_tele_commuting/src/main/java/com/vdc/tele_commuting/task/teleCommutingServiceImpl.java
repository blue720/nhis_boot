package com.vdc.tele_commuting.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("teleCommutingService")
public class teleCommutingServiceImpl implements TeleCommutingService{
    Logger logger = LoggerFactory.getLogger(this.getClass());

    
    @Resource(name="teleCommutingDAO")
    private TeleCommutingDAO teleCommutingDAO;

    @Override
    public List<Map<String,Object>> getZ06Data()throws Exception {
    	return teleCommutingDAO.getZ06Data();
    }
    
    @Override
    public void setTBCTAB03(Map<String,Object> map)throws Exception {
        teleCommutingDAO.setTBCTAB03(map);
    }
    
    @Override
    public void setProcY(Map<String,Object> map)throws Exception {
        teleCommutingDAO.setProcY(map);
    }
      
    
    
}
